DROP TABLE IF EXISTS worker CASCADE;
DROP TABLE IF EXISTS products CASCADE;
DROP TABLE IF EXISTS supplements CASCADE;
DROP TABLE IF EXISTS tasks CASCADE;
DROP TABLE IF EXISTS clients;
DROP TABLE IF EXISTS kitchens;
DROP EXTENSION IF EXISTS pgcrypto;
DROP TRIGGER IF EXISTS readiness_check ON tasks;

CREATE EXTENSION pgcrypto;

CREATE TABLE worker(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    full_name VARCHAR(50)UNIQUE,
    salary INT
);

INSERT INTO worker (full_name, salary) VALUES ('stepan kravchenko', 5500),
       ('oleg dubrov', 7000),
       ('karina denisova', 12000),
       ('olga hizhnyak', 9000),
       ('oleksey singaenko', 10000);

CREATE TABLE kitchens(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    kitchen_name VARCHAR(10) UNIQUE
);

INSERT INTO kitchens (kitchen_name) VALUES ( 'kitchen 1'),
( 'kitchen 2'),
( 'kitchen 3');

CREATE TABLE clients (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    full_name VARCHAR(50)UNIQUE,
    phone VARCHAR(20),
    address_street VARCHAR(50),
    address_building VARCHAR(10)
);

INSERT INTO clients (full_name, phone, address_street, address_building) VALUES ('dmitriy ivanov', '0683860932', 'ivanova', '30'),
('sergey dondik', '0683860932', 'magistralnaya', '77'),
('olga lazarevich', '0978541236', 'pishevaya', '13'),
('pavel poznyak', '0668534721', 'svobodi', '4'),
('nikita basov', '0669858741', 'sveta', '81'),
('ekaterina hizhyak', '0508741478', '8 marta', '3');

CREATE TABLE products(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    product_name VARCHAR(20) UNIQUE,
    price float,
    contains_tomatoes BOOLEAN,
    kitchen_name VARCHAR(10) REFERENCES kitchens(kitchen_name)
);

INSERT INTO products (product_name, price, contains_tomatoes, kitchen_name) VALUES ('sushi', 80, false, 'kitchen 1'),
('burger', 40, true, 'kitchen 2'),
('big mac', 60, true, 'kitchen 1'),
('pizza', 20, true, 'kitchen 3'),
('chiken naggets', 35, false, 'kitchen 1');

CREATE TABLE supplements(
    id  UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    supplement_name VARCHAR(20) UNIQUE,
    price float,
    contains_milk BOOLEAN
);

INSERT INTO supplements(supplement_name, price, contains_milk) VALUES ('sauce', 10, true),
('salat', 5, false),
('pineapple', 20, false),
('cheese', 10, true),
('mushrooms', 10, false);

CREATE TABLE tasks(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    number SERIAL,
    client_name VARCHAR(20) REFERENCES clients(full_name),
    lead_time timestamptz,

    product_name VARCHAR(20) REFERENCES products(product_name),
    product_quantity INT,
    supplement_name VARCHAR(20) REFERENCES supplements(supplement_name),
    supplement_quantity INT,

    worker_name VARCHAR(50) REFERENCES worker(full_name),
    completed boolean DEFAULT false,
    delivered boolean  DEFAULT false   
);

CREATE OR REPLACE FUNCTION readiness_check_func( ) RETURNS TRIGGER AS $$
BEGIN
    IF (NEW.delivered=true) THEN
        IF (OLD.completed=false) THEN
            NEW.delivered=false;
            RETURN NEW;
        ELSIF (OLD.completed=true) THEN
            NEW.delivered=true;
            RETURN NEW;
        END IF;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER readiness_check BEFORE INSERT OR UPDATE
    ON tasks
    FOR EACH ROW
    EXECUTE PROCEDURE readiness_check_func();

----------------- To be completed by the account manager ----------------
INSERT INTO tasks (client_name, lead_time, product_name, product_quantity, supplement_name, supplement_quantity) 
VALUES ('pavel poznyak', '2020-08-15 16:00:00', 'pizza', 1, 'pineapple', 1),
('ekaterina hizhyak', '2020-08-15 16:30:00', 'big mac', 3, 'mushrooms', 1),
('pavel poznyak', '2020-08-15 18:00:00', 'burger', 1, 'cheese', 2),
('olga lazarevich', '2020-08-15 19:30:00', 'pizza', 1, 'sauce', 1),
('dmitriy ivanov', '2020-08-15 20:00:00', 'big mac', 1, 'pineapple', 1),
('ekaterina hizhyak', '2020-08-15 22:00:00', 'burger', 1, 'cheese', 1);

--------------------- To be completed by the kitchen manager -------------
UPDATE tasks SET worker_name='stepan kravchenko' WHERE number=1;
UPDATE tasks SET worker_name='oleg dubrov' WHERE number=2;
UPDATE tasks SET worker_name='stepan kravchenko' WHERE number=3;
UPDATE tasks SET worker_name='karina denisova' WHERE number=4;
UPDATE tasks SET worker_name='oleksey singaenko' WHERE number=5;
UPDATE tasks SET worker_name='karina denisova' WHERE number=6;

UPDATE tasks SET completed=true WHERE number=5;
UPDATE tasks SET completed=true WHERE number=6;

--------------------- To be completed by the delivery service -------------
UPDATE tasks SET delivered=true WHERE number=6;
UPDATE tasks SET delivered=true WHERE number=4;

------------ All orders ---------
--SELECT * FROM tasks;

----------- Information about the stage of completion of the 1st order ------------
SELECT * FROM tasks t LEFT JOIN clients c ON t.client_name=c.full_name WHERE client_name='pavel poznyak';
